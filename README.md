# com2tcp

#### 介绍
一个JFX小程序，实现COM与TCP/IP互通

#### 软件架构
jSerialComm 实现串口通信

netty 转发串口和TCP


#### 安装教程

启动Main方法即可运行

打包可用 exe4j 或 javafx-maven-plugin

#### 使用说明

使用场景，服务在云端，需要连接本地设备时。

也可改造成插件的形式，界面写在WEB上，用户只需第一次使用安装即可。

![软件截图](https://images.gitee.com/uploads/images/2020/0817/143536_99bb2876_5129310.png "软件截图.png")

