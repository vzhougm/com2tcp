package com.oweight.com2tpc;

import cn.hutool.core.util.StrUtil;
import com.oweight.com2tpc.connect.serial.SerialClient;
import com.oweight.com2tpc.connect.tcp.TCPClient;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Controller implements Initializable {

    @FXML
    public TextField serialPort;

    @FXML
    public TextField baudRate;

    @FXML
    public Button comConnect;

    @FXML
    public Button comDisconnect;

    @FXML
    public TextField host;

    @FXML
    public TextField port;

    @FXML
    public Button tcpConnect;

    @FXML
    public Button tcpDisconnect;

    @FXML
    public Circle comStatus;

    @FXML
    public Circle tcpStatus;

    public static String serialPortStr;
    public static String baudRateStr;

    public static String hostStr;
    public static String portStr;

    public static volatile boolean comInitiativeDisconnect;
    public static volatile boolean tcpInitiativeDisconnect;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // 串口连接
        comConnect.setOnAction(event -> {
            comInitiativeDisconnect = false;
            connectionSucceeded(null, comConnect);

            serialPortStr = serialPort.getText();
            baudRateStr = baudRate.getText();

            if (StrUtil.isNotBlank(serialPortStr) && StrUtil.isNotBlank(baudRateStr)) {
                Executors.newSingleThreadExecutor().execute(() -> {
                    try {
                        SerialClient.start(this);
                    } catch (Exception ignored) {
                    }
                });
            }

        });

        comDisconnect.setOnAction(event -> {
            SerialClient.stop();
            connectionFailed(comStatus, comConnect);
            comInitiativeDisconnect = true;
        });

        // TCP/IP
        tcpConnect.setOnAction(event -> {
            tcpInitiativeDisconnect = false;
            connectionSucceeded(null, tcpConnect);

            hostStr = host.getText();
            portStr = port.getText();

            if (StrUtil.isNotBlank(hostStr) && StrUtil.isNotBlank(portStr)) {
                Executors.newSingleThreadExecutor().execute(() -> {
                    try {
                        TCPClient.start(this);
                    } catch (Exception ignored) {

                    }
                });
            }
        });

        tcpDisconnect.setOnAction(event -> {
            TCPClient.stop();
            connectionFailed(tcpStatus, tcpConnect);
            tcpInitiativeDisconnect = true;
        });
    }

    /**
     * 连接断开
     *
     * @param status
     * @param connect
     */
    public static void connectionFailed(Circle status, Button connect) {
        if (status != null) status.setFill(Color.GRAY);
        if (connect != null) connect.setDisable(false);
    }

    /**
     * 连接成功
     *
     * @param status
     * @param connect
     */
    public static void connectionSucceeded(Circle status, Button connect) {
        if (status != null) status.setFill(Color.LAWNGREEN);
        if (connect != null) connect.setDisable(true);
    }


    /**
     * 设置串口数据上传状态
     */
    public void setComDataUp() {
        try {
            comStatus.setFill(Color.BLUE);
            TimeUnit.MILLISECONDS.sleep(100);
            comStatus.setFill(Color.LAWNGREEN);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置TCP数据上传状态
     */
    public void setTcpDataUp() {
        try {

            tcpStatus.setFill(Color.BLUE);
            TimeUnit.MILLISECONDS.sleep(100);
            tcpStatus.setFill(Color.LAWNGREEN);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置串口数据下行状态
     */
    public void setComDataDown() {
        try {
            comStatus.setFill(Color.ORANGERED);
            TimeUnit.MILLISECONDS.sleep(100);
            comStatus.setFill(Color.LAWNGREEN);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置TCP数据下行状态
     */
    public void setTcpDataDown() {
        try {

            tcpStatus.setFill(Color.ORANGERED);
            TimeUnit.MILLISECONDS.sleep(100);
            tcpStatus.setFill(Color.LAWNGREEN);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
