package com.oweight.com2tpc.connect;

public interface Constant {
    String COM_OPEN = "COM#OPEN";
    String COM_CLOSED = "COM#CLOSED";
    String TPC_OPEN = "TPC/IP#OPEN";
    String TPC_CLOSED = "TPC/IP#CLOSED";
}
