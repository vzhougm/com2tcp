package com.oweight.com2tpc.connect.serial;

import com.oweight.com2tpc.Controller;
import com.oweight.com2tpc.connect.Constant;
import com.oweight.com2tpc.connect.serial.SerialClient;
import com.oweight.com2tpc.connect.tcp.TCPClient;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SerialDataHandler extends ChannelDuplexHandler {

    private final Controller controller;

    public SerialDataHandler(Controller controller1) {
        this.controller = controller1;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        super.write(ctx, msg, promise);
        controller.setComDataDown();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        try {
            controller.setComDataUp();
            TCPClient.channel.writeAndFlush(msg);
        } catch (Exception ignored) {
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        log.info("COM连接成功！");

        if (TCPClient.channel != null) {
            ctx.channel().writeAndFlush(Constant.TPC_OPEN.getBytes());
            TCPClient.channel.writeAndFlush(Constant.COM_OPEN.getBytes());
        } else {
            ctx.channel().writeAndFlush(Constant.TPC_CLOSED.getBytes());
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        log.info("COM断开连接！");

        if (TCPClient.channel != null) {
            TCPClient.channel.writeAndFlush(Constant.COM_CLOSED.getBytes());
        }

        SerialClient.channel = null;
    }

}
