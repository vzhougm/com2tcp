/*
 * Copyright 2018 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.oweight.com2tpc.connect.serial.jserialcomm;

import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.*;

import java.util.Map;

/**
 * Default configuration class for jSerialComm device connections.
 */
final class DefaultJSerialCommChannelConfig extends DefaultChannelConfig implements JSerialCommChannelConfig {

    private volatile int baudrate = 115200;
    private volatile boolean dtr;
    private volatile boolean rts;
    private volatile Stopbits stopbits = Stopbits.ONE_STOP_BIT;
    private volatile int databits = 8;
    private volatile Paritybit paritybit = Paritybit.NO_PARITY;
    private volatile int waitTime= 3;
    private volatile int readTimeout = 1000;
    private volatile int writeTimeout = 0;
    private volatile int safetySleepTime = 200;
    private volatile int deviceSendQueueSize = 4096;
    private volatile int deviceReceiveQueueSize = 10240;

    DefaultJSerialCommChannelConfig(JSerialCommChannel channel) {
        super(channel);
        setAllocator(new PreferHeapByteBufAllocator(getAllocator()));
    }

    @Override
    public Map<ChannelOption<?>, Object> getOptions() {
        return getOptions(super.getOptions(), JSerialCommChannelOption.BAUD_RATE, JSerialCommChannelOption.DTR, JSerialCommChannelOption.RTS, JSerialCommChannelOption.STOP_BITS, JSerialCommChannelOption.DATA_BITS, JSerialCommChannelOption.PARITY_BIT, JSerialCommChannelOption.WAIT_TIME,
                JSerialCommChannelOption.SAFETY_SLEEP_TIME, JSerialCommChannelOption.DEVICE_SEND_QUEUE_SIZE, JSerialCommChannelOption.DEVICE_RECEIVE_QUEUE_SIZE);
    }

    @Override
    public <T> T getOption(ChannelOption<T> option) {
        if (option == JSerialCommChannelOption.BAUD_RATE) {
            return (T) Integer.valueOf(getBaudrate());
        }
        if (option == JSerialCommChannelOption.DTR) {
            return (T) Boolean.valueOf(isDtr());
        }
        if (option == JSerialCommChannelOption.RTS) {
            return (T) Boolean.valueOf(isRts());
        }
        if (option == JSerialCommChannelOption.STOP_BITS) {
            return (T) getStopbits();
        }
        if (option == JSerialCommChannelOption.DATA_BITS) {
            return (T) Integer.valueOf(getDatabits());
        }
        if (option == JSerialCommChannelOption.PARITY_BIT) {
            return (T) getParitybit();
        }
        if (option == JSerialCommChannelOption.WAIT_TIME) {
            return (T) Integer.valueOf(getWaitTimeMillis());
        }
        if (option == JSerialCommChannelOption.READ_TIMEOUT) {
            return (T) Integer.valueOf(getReadTimeout());
        }
        if (option == JSerialCommChannelOption.WRITE_TIMEOUT) {
            return (T) Integer.valueOf(getWriteTimeout());
        }
        if (option == JSerialCommChannelOption.SAFETY_SLEEP_TIME) {
            return (T) Integer.valueOf(getSafetySleepTime());
        }
        if (option == JSerialCommChannelOption.DEVICE_SEND_QUEUE_SIZE) {
            return (T) Integer.valueOf(getDeviceSendQueueSize());
        }
        if (option == JSerialCommChannelOption.DEVICE_RECEIVE_QUEUE_SIZE) {
            return (T) Integer.valueOf(getDeviceReceiveQueueSize());
        }
        return super.getOption(option);
    }

    @Override
    public <T> boolean setOption(ChannelOption<T> option, T value) {
        validate(option, value);

        if (option == JSerialCommChannelOption.BAUD_RATE) {
            setBaudrate((Integer) value);
        } else if (option == JSerialCommChannelOption.DTR) {
            setDtr((Boolean) value);
        } else if (option == JSerialCommChannelOption.RTS) {
            setRts((Boolean) value);
        } else if (option == JSerialCommChannelOption.STOP_BITS) {
            setStopbits((Stopbits) value);
        } else if (option == JSerialCommChannelOption.DATA_BITS) {
            setDatabits((Integer) value);
        } else if (option == JSerialCommChannelOption.PARITY_BIT) {
            setParitybit((Paritybit) value);
        } else if (option == JSerialCommChannelOption.WAIT_TIME) {
            setWaitTimeMillis((Integer) value);
        } else if (option == JSerialCommChannelOption.READ_TIMEOUT) {
            setReadTimeout((Integer) value);
        } else if (option == JSerialCommChannelOption.WRITE_TIMEOUT) {
            setWriteTimeout((Integer) value);
        } else if (option == JSerialCommChannelOption.SAFETY_SLEEP_TIME) {
            setSafetySleepTime((Integer) value);
        } else if (option == JSerialCommChannelOption.DEVICE_SEND_QUEUE_SIZE) {
            setDeviceSendQueueSize((Integer) value);
        } else if (option == JSerialCommChannelOption.DEVICE_RECEIVE_QUEUE_SIZE) {
            setDeviceReceiveQueueSize((Integer) value);
        } else {
            return super.setOption(option, value);
        }
        return true;
    }

    @Override
    public JSerialCommChannelConfig setBaudrate(final int baudrate) {
        this.baudrate = baudrate;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setStopbits(final Stopbits stopbits) {
        this.stopbits = stopbits;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setDatabits(final int databits) {
        this.databits = databits;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setParitybit(final Paritybit paritybit) {
        this.paritybit = paritybit;
        return this;
    }

    @Override
    public int getBaudrate() {
        return baudrate;
    }

    @Override
    public Stopbits getStopbits() {
        return stopbits;
    }

    @Override
    public int getDatabits() {
        return databits;
    }

    @Override
    public Paritybit getParitybit() {
        return paritybit;
    }

    @Override
    public boolean isDtr() {
        return dtr;
    }

    @Override
    public JSerialCommChannelConfig setDtr(final boolean dtr) {
        this.dtr = dtr;
        return this;
    }

    @Override
    public boolean isRts() {
        return rts;
    }

    @Override
    public JSerialCommChannelConfig setRts(final boolean rts) {
        this.rts = rts;
        return this;
    }

    @Override
    public int getWaitTimeMillis() {
        return waitTime;
    }

    @Override
    public int getSafetySleepTime() {
        return safetySleepTime;
    }

    @Override
    public int getDeviceSendQueueSize() {
        return deviceSendQueueSize;
    }

    @Override
    public int getDeviceReceiveQueueSize() {
        return deviceReceiveQueueSize;
    }

    @Override
    public JSerialCommChannelConfig setSafetySleepTime(int safetySleepTime) {
        this.safetySleepTime = safetySleepTime;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setDeviceSendQueueSize(int deviceSendQueueSize) {
        this.deviceSendQueueSize = deviceSendQueueSize;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setDeviceReceiveQueueSize(int deviceReceiveQueueSize) {
        this.deviceReceiveQueueSize = deviceReceiveQueueSize;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setWaitTimeMillis(final int waitTimeMillis) {
        if (waitTimeMillis < 0) {
            throw new IllegalArgumentException("Wait time must be >= 0");
        }
        waitTime = waitTimeMillis;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setReadTimeout(int readTimeout) {
        if (readTimeout < 0) {
            throw new IllegalArgumentException("readTimeout must be >= 0");
        }
        this.readTimeout = readTimeout;
        return this;
    }

    @Override
    public JSerialCommChannelConfig setWriteTimeout(int writeTimeout) {
        this.writeTimeout = writeTimeout;
        return this;
    }

    @Override
    public int getReadTimeout() {
        return readTimeout;
    }

    @Override
    public int getWriteTimeout() {
        return writeTimeout;
    }

    @Override
    public JSerialCommChannelConfig setConnectTimeoutMillis(int connectTimeoutMillis) {
        super.setConnectTimeoutMillis(connectTimeoutMillis);
        return this;
    }

    @Override
    @Deprecated
    public JSerialCommChannelConfig setMaxMessagesPerRead(int maxMessagesPerRead) {
        super.setMaxMessagesPerRead(maxMessagesPerRead);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setWriteSpinCount(int writeSpinCount) {
        super.setWriteSpinCount(writeSpinCount);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setAllocator(ByteBufAllocator allocator) {
        super.setAllocator(allocator);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setRecvByteBufAllocator(RecvByteBufAllocator allocator) {
        super.setRecvByteBufAllocator(allocator);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setAutoRead(boolean autoRead) {
        super.setAutoRead(autoRead);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setAutoClose(boolean autoClose) {
        super.setAutoClose(autoClose);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setWriteBufferHighWaterMark(int writeBufferHighWaterMark) {
        super.setWriteBufferHighWaterMark(writeBufferHighWaterMark);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setWriteBufferLowWaterMark(int writeBufferLowWaterMark) {
        super.setWriteBufferLowWaterMark(writeBufferLowWaterMark);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setWriteBufferWaterMark(WriteBufferWaterMark writeBufferWaterMark) {
        super.setWriteBufferWaterMark(writeBufferWaterMark);
        return this;
    }

    @Override
    public JSerialCommChannelConfig setMessageSizeEstimator(MessageSizeEstimator estimator) {
        super.setMessageSizeEstimator(estimator);
        return this;
    }
}
