package com.oweight.com2tpc;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.net.URL;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {

        this.enableTray(primaryStage);

        URL fxml1 = getClass().getClassLoader().getResource("main.fxml");
        assert fxml1 != null;
        Parent root = FXMLLoader.load(fxml1);

        primaryStage.setTitle("COM with TCP/IP");
        primaryStage.setScene(new Scene(root, 550, 225));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new javafx.scene.image.Image("icon.png"));
        primaryStage.show();

        Platform.setImplicitExit(false);

    }

    //右小角,最小化.
    private void enableTray(final Stage stage) {
        PopupMenu popupMenu = new PopupMenu();
        java.awt.MenuItem openItem = new java.awt.MenuItem("show");
        java.awt.MenuItem hideItem = new java.awt.MenuItem("hide");
        java.awt.MenuItem quitItem = new java.awt.MenuItem("exit");

        ActionListener acl = e -> {
            MenuItem item = (MenuItem) e.getSource();
            Platform.setImplicitExit(false); //多次使用显示和隐藏设置false

            if (item.getLabel().equals("show")) {
                Platform.runLater(stage::show);
            }

            if (item.getLabel().equals("hide")) {
                Platform.runLater(stage::hide);
            }

            if (item.getLabel().equals("exit")) {
                Platform.exit();
                System.exit(0);
            }

        };

        //双击事件方法
        MouseListener sj = new MouseListener() {
            public void mouseReleased(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseClicked(MouseEvent e) {
                Platform.setImplicitExit(false); //多次使用显示和隐藏设置false
                if (e.getClickCount() == 2) {
                    if (stage.isShowing()) {
                        Platform.runLater(stage::hide);
                    } else {
                        Platform.runLater(stage::show);
                    }
                }
            }
        };


        openItem.addActionListener(acl);
        quitItem.addActionListener(acl);
        hideItem.addActionListener(acl);

        popupMenu.add(openItem);
        popupMenu.add(hideItem);
        popupMenu.add(quitItem);

        try {
            SystemTray tray = SystemTray.getSystemTray();

            URL resource = getClass().getResource("/icon.png");
            BufferedImage image = ImageIO.read(resource);
            TrayIcon trayIcon = new TrayIcon(image, "COM with TCP/IP", popupMenu);
            tray.add(trayIcon);
            trayIcon.addMouseListener(sj);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
